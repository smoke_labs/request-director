# Issue Template

## Summary
Briefly describe the issue. Provide enough detail so anyone can understand the issue without additional context.

## Steps to Reproduce
List the steps to reproduce the behavior:
1. Go to '...'
2. Click on '....'
3. Scroll down to '....'
4. See error

## Expected Behavior
Describe what you expected to happen.

## Actual Behavior
Describe what actually happened. Include screenshots if applicable.

## Possible Fix
If you have a suggestion for how to fix the issue, please describe it here.

## Additional Context
Add any other context about the problem here. Include things like the browser version, or the time of day the issue occurs.

## Relevant Logs and/or Screenshots
```text
Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.

/label ~"kind/bug"
/assign @username